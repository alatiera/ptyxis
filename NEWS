# Ptyxis 47.alpha

This is our first pre-release for GNOME 47 as part of 47.alpha.

Chances since 46.0

 * Many updates for GNOME HIG across dialogs and widgetry.
 * Ptyxis can run the ptyxis-agent inside the sandbox if we fail to run
   on the host. That comes with drawbacks but at least keeps the application
   working in those scenarios.
 * The build system supports some whitebox renaming allowing it to be called
   "Terminal" in some situations. Distributions shipping Ptyxis as their
   terminal may be encouraged to use that. Branding is also reduced to less
   dramatic styling.
 * Many fixes for Podman, Toolbox, and Distrobox.
 * Fallback to `sh` if we cannot locate the shell specified in /etc/passwd.
 * Improvements to the tab parking lot so that processes are properly exited.
 * Notification improvements.
 * Port Ptyxis to use the new VTE "termprops" feature. This requires a newer
   VTE than is currently released but is anticipated shortly. This reduces
   the number of patches required by Ptyxis (and eventually Fedora) to a
   single small patch for vte.sh.in.
 * New palettes styles.
 * Many new command line options and improved support for combining them.
 * Translation updates.

Thanks to everyone who filed bugs and contributed patches!

-- Christian

